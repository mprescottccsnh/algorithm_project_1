#Algorithm Project 1#

Using the flowcharts, pseudocode and Python code provided, create completed Python applications for each of the following:

##Twelve Days of Christmas##
Using the TwelveDays class file provided along with the flowchart image below, create a Python script to generate the Christmas song, The Twelve Days of Christmas. If you have already completed this challenge a different way, adjust your solution so that it follows the logic of the flowchart:
http://www.gliffy.com/go/publish/image/5365347/L.png

##Floyd's Triangle Algorithm##
Read the information provided at this web site:
http://www.codewithc.com/floyds-triangle-algorithm-flowchart/

Create a Python script that asks the user to provide a positive integer value, and then generate the appropriate Floyd's Triangle. In addition to printing the individual values for each row, also print the sum of those integers and show that each sum is equal to:

n(n^2 + 1)/2

##UPDATE##

An improved version of the Floyd's Triangle Flowchart can be found here:
http://www.gliffy.com/go/publish/image/9733965/L.png